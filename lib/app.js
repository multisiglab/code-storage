require('babel-register')
const exec = require('child_process').exec
const eos = require('../integration/eos_connector')

const contractHolder = "ivanver";
const account = "nik";

let peerId = null;

async function sh(cmd) {
    return new Promise(function (resolve, reject) {
        exec(cmd, (err, stdout, stderr) => {
            if (err) {
                reject(err);
            } else {
                stdout = stdout.substring(0, stdout.length-1)
                resolve({ stdout, stderr });
            }
        });
    });
}

async function startNode() {
    let { stdout } = await sh('ipfs id')
    await sh('ipfs bootstrap rm --all')

    eos.getTableRows({
        "json": true,
        "code": contractHolder,
        "scope": contractHolder,
        "table": "storages",
        "table_key": "uint64"
    }).then(res => {
        res.rows.forEach(element => {
            let node = `/ip4/${element.ip}/tcp/4001/ipfs/${element.peerid}`
            sh(`ipfs bootstrap add ${node}`)
        });
    }).then(() => {
        let out = JSON.parse(stdout)
        let addresses = out.Addresses[out.Addresses.length - 1]

        console.log("IPFS address of the remote code storage: ", addresses);

        peerId = out.ID
        let ip = addresses.substring(5, addresses.indexOf('\/', 5))
        return {ip, peerId}
    }).then((res) => {
        let {ip, peerId} = res

        console.log("IP address of code storage: ", ip)
        console.log("IPFS peer ID: ", peerId)

        return eos.transaction({
            actions: [{
                account: contractHolder,
                name: 'addstorage',
                authorization: [{
                    actor: account,
                    permission: 'active'
                }],
                data: {
                    worker: account,
                    ip: ip,
                    peerid: peerId
                }
            }]
        })
    }).then(res => {
        console.log("--- START OBSERIVNG FOR NEW REPOSITORIES TO HOST ---")
        startCheckRepo()
    })
}

startNode()

async function checkAvailableRepo() {
    eos.getTableRows({
        "json": true,
        "code": contractHolder,
        "scope": contractHolder,
        "table": "repos",
        "table_key": "uint64"
    }).then(res => {
        console.log(res)
        
        res.rows.forEach(row => {
            if (row.status != 0)
                return

            for (let h of row.hosts) {
                if (h.status != 0 || h.peerid != peerId)
                    return
                
                sh(`bash ./tools/host.sh ${row.userlink} ${peerId}`).then(res => {
                    let stdout = res.stdout

                    console.log("Repository name: ", row.reponame)
                    console.log("New address of code storage in IPFS: ", stdout)
                
                    eos.transaction({
                        actions: [{
                        account: contractHolder,
                        name: 'updatehost',
                        authorization: [{
                            actor: account,
                            permission: 'active'
                        }],
                        data: {
                            worker: account,
                            reponame: row.reponame,
                            hostlink: stdout
                        }
                        }]
                    }).then(res => {
                        startCheckRepo()
                    })
                })
            }
        })
    })
}

async function startCheckRepo() {
    setInterval(() => {
        checkAvailableRepo()
    }, 10000)
}

