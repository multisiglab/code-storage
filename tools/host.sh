repoId="$1"
peerId="$2"
path=$HOME/temp/$repoId

ipfs get -o $path $repoId > /dev/null

echo $peerId > "$path/hash.salt"
echo $(ipfs add -r -Q $path) | tail -n1
